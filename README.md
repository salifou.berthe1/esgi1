# Exercice Maven et Junit 



## Avant de commencer

Ce projet contient quelques exercices concernant maven et Junit. Vous devez, au pr�alable, avoir install� Maven 3.x.x.
Ci dessous les instrutions pour derouler les exercices qui doivent �tre faits dans l'ordre car ils sont dependents l'un de l'autre.
Vous devez aussi avoir git install� afin de recup�rer les exercices.

## Exercice

### Exercice 1
Dans cet exercice vous devez construire le projet avec la commande `` mvn clean install ``

```
[INFO] Compiling 4 source files to C:\PERSO\SABE-CONSULTING\ESGI\exam\projet\esgi1\target\classes
[INFO] -------------------------------------------------------------
[ERROR] COMPILATION ERROR : 
[INFO] -------------------------------------------------------------
[ERROR] Source option 5 is no longer supported. Use 7 or later.
[ERROR] Target option 5 is no longer supported. Use 7 or later.
[INFO] 2 errors 
[INFO] -------------------------------------------------------------
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.643 s
```
- Configurer le plugin ``compile `` [pour utiliser java 11](https://maven.apache.org/plugins/maven-compiler-plugin/examples/set-compiler-source-and-target.html) et crecompilez le projet


### Exercice 2
Ajouter la dependence suivante � votre projet

```
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.3.1</version>
            <scope>test</scope>
        </dependency>
```
Et ex�cuter la comment `` mvn clean install `` encore.

```
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  4.009 s
[INFO] Finished at: 2022-03-02T13:28:38+01:00
[INFO] ------------------------------------------------------------------------

```
Notez que Maven n'a besoin de t�l�charger les dependences cette fois ci, alors le buil �tait plus rapide.

Allez voir votre repository local ``~/.m2/repository/`` vous verez que les artifacts sont organis�s par hi�rarchi.  



### Exercice 3
Cr�ez une classe ``edu.esgi.al.project5.CalculatorTest.java" avec le code 

```
public class CalculatorTest {
  @Test
  public void check_pisitve_number_addition() {
	  Calculator cal = new Calculator();
	  int r = cal.add(1, 1);
	  assertEquals(2, r);
  }
}
```
Ex�cutez la `` mvn clean test`` et remarquez que le test n'a pas �t� ex�cut�

```
-------------------------------------------------------
 T E S T S
-------------------------------------------------------
Running edu.esgi.al.project5.CalculatorTest
Tests run: 0, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.002 sec

Results :

Tests run: 0, Failures: 0, Errors: 0, Skipped: 0
```
et lancez � nouveau la commande de test

```
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
[INFO] Running edu.esgi.al.project5.CalculatorTest
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.045 s - in edu.esgi.al.project5.CalculatorTest
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
```
et v�rifiez le r�sultat du test.

### Exercice 3
Utilisez la plugin Jacoco ``jacoco-maven-plugin`` afin de voir la couverture de vos tests.
Lancez la commande de test et allez dans le r�pertoire ``target\site\jacoco`` pour voir le r�sultat de votre couverture des  tests.

L'application n'est couverte qu'� 7 %. Il est recommend� d'avoir une couverture d'au moins 80%.

### Exercice 4:
Cr�ez et analysez la classe `` MoneyTransactionServiceTest ``
Enrichir cette classe afin d'avoir un taux de couverture de 80% min


```
public class MoneyTransactionServiceTest {
	
	private static final String MONEY_AMOUNT_EXCEPTION_MSG = "Money amount should be greater than 0";
	private static final String ACCOUNT_EXCEPTION_MSG = "Accounts shouldn't be null";
	private static final double RANDOM_MONEY_AMOUNT = 100;
	private static final double ZERO_MONEY_AMOUNT = 0;
	private static final double MORE_THAN_RANDOM_MONEY_AMOUNT = 200;
	private static final double NEGATIVE_MONEY_AMOUNT = -1;
	
	
	private MoneyTransactionService testInstance;
	
//	@Before 			//  JUnit 4
	@BeforeEach 		//  JUnit 5
	void setUp() {
		testInstance = new MoneyTransactionService();
	}
	
//	@After 			//  JUnit 4
	@AfterEach		//  JUnit 5
	void tearDown() {
		// cette methode sera execut� apr�s chaque methode de test
	}
	
//	@BeforeClass	//  JUnit 4 et   methode statique!
	@BeforeAll		//  JUnit 5 et   methode statique!
	static void beforeAll() {
		// cette methode sera executee avec les tests 
	}
	
//	@AfterClass //  JUnit 4
	@AfterAll	//  JUnit 5
	static void afterAll() {
		//cette methode sera executee avec les tests
	}
	
	@Test
//	void transferMoneyTest() {
	void shouldTransferMoneyFromOneAccountToAnother() {
		// GIVEN
		var account1 = new Account(RANDOM_MONEY_AMOUNT);
		var account2 = new Account(ZERO_MONEY_AMOUNT);
		
		// WHEN
		testInstance.transferMoney(account1, account2, RANDOM_MONEY_AMOUNT);
		
		// THEN
		assertEquals(ZERO_MONEY_AMOUNT, account1.getMoneyAmount());
		assertEquals(RANDOM_MONEY_AMOUNT, account2.getMoneyAmount());
	}
	

	@Test
	void shouldThrowExceptionIfAccountFromIsNull() {
		// GIVEN
		Account account1 = null;
		Account account2 = new Account(RANDOM_MONEY_AMOUNT);
		
		// WHEN & THEN
		var exception = assertThrows(IllegalArgumentException.class, () -> 
			testInstance.transferMoney(account1, account2, RANDOM_MONEY_AMOUNT)
		);
		
		assertEquals(ACCOUNT_EXCEPTION_MSG, exception.getMessage());
	}
	
	/*@org.junit.Test(expected = IllegalArgumentException.class)  // JUNIT 4
	public void shouldThrowExceptionIfAccountFromIsNull2() {
		// GIVEN
		Account account1 = null;
		Account account2 = new Account(RANDOM_MONEY_AMOUNT);
		testInstance = new MoneyTransactionService();
		
		// WHEN & THEN
		testInstance.transferMoney(account1, account2, RANDOM_MONEY_AMOUNT);
	}*/
	
	@Test
	void shouldThrowExceptionIfAccountToIsNull() {
		// GIVEN
		Account account1 = new Account(RANDOM_MONEY_AMOUNT);
		Account account2 = null;
		
		// WHEN & THEN
		var exception = assertThrows(IllegalArgumentException.class, () -> 
			testInstance.transferMoney(account1, account2, RANDOM_MONEY_AMOUNT)
		);
		
		assertEquals(ACCOUNT_EXCEPTION_MSG, exception.getMessage());
	}
	
	@Test
	void shouldThrowNotEnoughMoneyExceptionWhenTransferMoreMoney() {
		// GIVEN
		var account1 = new Account(RANDOM_MONEY_AMOUNT);
		var account2 = new Account(ZERO_MONEY_AMOUNT);
		
		// WHEN
		assertThrows(NotEnoughMoneyException.class, () -> 
			testInstance.transferMoney(account1, account2, MORE_THAN_RANDOM_MONEY_AMOUNT)
		);
		
	}
	
	@Test
	void shouldThrowExcpetionWhenTransferNegativeAmount() {
		// GIVEN
		var account1 = new Account();
		var account2 = new Account();
		
		// WHEN
		var exception = assertThrows(IllegalArgumentException.class, () -> 
					testInstance.transferMoney(account1, account2, NEGATIVE_MONEY_AMOUNT)
				);
		
		assertEquals(MONEY_AMOUNT_EXCEPTION_MSG, exception.getMessage());
	}
	
	@Test
	void shouldThrowExcpetionWhenTransferZeroMoneyAmount() {
		// GIVEN
		var account1 = new Account();
		var account2 = new Account();

		// WHEN
		var exception = assertThrows(IllegalArgumentException.class, () -> 
					testInstance.transferMoney(account1, account2, ZERO_MONEY_AMOUNT)
				);
		
		assertEquals(MONEY_AMOUNT_EXCEPTION_MSG, exception.getMessage());
	}
}

```


