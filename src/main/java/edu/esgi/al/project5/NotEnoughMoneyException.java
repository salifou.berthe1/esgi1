package edu.esgi.al.project5;

public class NotEnoughMoneyException extends RuntimeException {

	public NotEnoughMoneyException() {
		super();
	}

	public NotEnoughMoneyException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public NotEnoughMoneyException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotEnoughMoneyException(String message) {
		super(message);
	}

	public NotEnoughMoneyException(Throwable cause) {
		super(cause);
	}

}
